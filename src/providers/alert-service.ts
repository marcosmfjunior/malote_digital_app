import { AlertController, ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the AlertService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class MessageService {

  constructor(public http: Http,public alertCtrl: AlertController, public toastCtrl:ToastController) {
    console.log('Hello AlertService Provider');
  }

  showAlert(title, subTitle, buttonObj){
    if(buttonObj == null){
      buttonObj = ["ok"];

      let Alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: buttonObj
      })

    console.log(Alert);
    Alert.present();
    }
  }

  showToast(message){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
}
