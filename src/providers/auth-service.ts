import { JwtHelper } from 'angular2-jwt';
import { AppSettings } from './../app/appSetings';
import { Events, LoadingController } from 'ionic-angular';
import { ShareService } from './share-service';
import { Injectable } from '@angular/core';
import { Http, Headers,RequestOptions,Response,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

import { User } from './../models/models';
/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

@Injectable()
export class AuthService {
  private token: any;
  private optionsHeader;
  private headers: Headers = this.getHeaders();
  // public url = 'https://burgerhouse.herokuapp.com' ;
  public url = AppSettings.API_ENDPOINT ;
  //public url ='https://infinity360br.herokuapp.com';


  private userObserver: any;
  private tokenObserver:any;
  public user: User ;
  public hasToken:any;
  public _user:User;
  public _token:any;
  public loading:any;

  constructor(public http: Http, public storage: Storage,public shareSvc:ShareService, public jwtHelper:JwtHelper ,public events: Events,  private loadingCtrl:LoadingController) {
    console.log(this.url);

    this.loading = this.loadingCtrl.create({
      content: 'Carregando'
    });

    this.tokenObserver = null;
    this.hasToken = Observable.create(observer => {
      this.tokenObserver = observer;
    });
  }

  getHeaders(): Headers {
    return new Headers({
        'Content-Type': 'application/json',
    });
  }

  private handleUser(user) {
    this.loading.dismiss();

    return this.userObserver.next(user) ;
  }

  handleError(error) {
    this.loading.dismiss();
    console.log(error);
    return error.json().message || 'Server error, please try again later';
  }

  setToken(value){
    console.log('set token')
    this._token = value;
    this.storage.set('token',value).then(res=>{

      let decodedToken = this.jwtHelper.decodeToken(res);
      this.shareSvc.setDecodedToken(decodedToken);
    })
  }


  // createAccount(params){
  //   this.loading.present();

  //   return this.http.post(this.url+'/signupuser', params)
  //     .map((res: Response) => {
  //       this.loading.dismiss();
  //       return res.json();
  //     });
  // }

  // forgotAccount(params){
  //   console.log(params)
  //   // this.loading.present();

  //   return this.http.post(this.url+'/forgot', params)
  //     .map((res: Response) => {
  //       this.loading.dismiss();
  //       return res.json();
  //     });
  // }

  login(credentials) {
    console.log(credentials)
    this.loading.present();

    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({ headers: headers });

    // let body = JSON.stringify(credentials);
    let body = new URLSearchParams();
    body.set('id', credentials.id);
    body.set('password', credentials.password);

    return this.http.post(this.url+'/login',body)
      .map((res: Response) => {
        this.loading.dismiss();
        return res.json();
      });
  }

  logout(){
    // this.storage.set('token', '');
    // this.storage.set('user', '');
  }

}
