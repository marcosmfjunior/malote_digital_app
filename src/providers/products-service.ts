import { Additional, Product } from './../models/models';
import { AppSettings } from './../app/appSetings';
import { LoadingController } from 'ionic-angular';
import { AuthConfig, AuthHttp } from 'angular2-jwt';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the AdditionalService provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductsService {

  private productsObservable: Observable<Product[]>;

  constructor(public http: Http, public authHttp:AuthHttp) {
    console.log('Hello ProposalServiceProvider Provider');
  }

  sendProduct(data) {
    return this.authHttp
      .post(AppSettings.API_ENDPOINT + "/product/", data)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  getProductsDB(type){
    console.log(type);
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/product/?type=" + type)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  getHistory(productId){
    console.log(productId);
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/product/history/orders?productId=" + productId)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  // getPortsAsync(page?: number, size?: number): Observable<Product[]> {
  //   if (this.productsObservable) {
  //     return this.productsObservable;
  //   }

  //   this.productsObservable = new Observable<Product[]>(observer => {
  //     console.log("VAI BUSCAR");
  //     this.getProductsDB().then((prods) => {
  //       observer.next(prods);
  //       observer.complete();
  //     })
  //     .catch((err)=> {
  //       console.log(err);
  //       observer.next(this.products);
  //       observer.complete();
  //     })
  //   });

  //   this.productsObservable.subscribe(() => {
  //     // Remove completed observable.
  //     this.productsObservable = null;
  //   });

  //   return this.productsObservable;
  // }

  // getProducts(){
  //   return this.products;
  // }

  // filterProducts(products: Product[], text: string): Product[] {

  //   return products.filter(product => {
  //     return product.name.toLowerCase().indexOf(text) !== -1;
  //   });
  // }


}
