import { LoadingController } from 'ionic-angular';
import { AuthHttp } from 'angular2-jwt';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from './../../app/appSetings';
import { FileTransferObject, FileTransfer } from '@ionic-native/file-transfer';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/*
  Generated class for the DfServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DfServiceProvider {
  public data: any;
  dfs: Observable<any[]>
  private _dfs: BehaviorSubject<any[]>;
  private dataStore: {
    dfs: any[]
  };
  public loading:any;
  public url = AppSettings.API_ENDPOINT+'/order/';

  constructor(public http: Http,private authHttp: AuthHttp,public storage:Storage, public loadingCtrl:LoadingController,private transfer: FileTransfer) {
  }

  getOpensDf(){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/order/")
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  getOpensDfByUser(){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/order/open-df/by-user")
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  getDf(id){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/order/"+id)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  sendOrder(data) {
    return this.authHttp
      .post(AppSettings.API_ENDPOINT + "/order/", data)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

//   createDf2(file){
//     window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
//     console.log('file system open: ' + fs.name);
//     fs.root.getFile('bot.png', { create: true, exclusive: false }, function (fileEntry) {
//         fileEntry.file(function (file) {
//             var reader = new FileReader();
//             reader.onloadend = function() {
//                 // Create a blob based on the FileReader "result", which we asked to be retrieved as an ArrayBuffer
//                 var blob = new Blob([new Uint8Array(this.result)], { type: "image/png" });
//                 var oReq = new XMLHttpRequest();
//                 oReq.open("POST", "http://mysweeturl.com/upload_handler", true);
//                 oReq.onload = function (oEvent) {
//                     // all done!
//                 };
//                 // Pass the blob in to XHR's send method
//                 oReq.send(blob);
//             };
//             // Read the file as an ArrayBuffer
//             reader.readAsArrayBuffer(file);
//         }, function (err) { console.error('error getting fileentry file!' + err); });
//     }, function (err) { console.error('error getting file! ' + err); });
// }, function (err) { console.error('error getting persistent fs! ' + err); });
//   }


}
