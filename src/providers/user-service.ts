import { ShareService } from './share-service';
import { AppSettings } from './../app/appSetings';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the UserService provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserService {

  constructor(public http: Http,private authHttp: AuthHttp,public shareSvc:ShareService) {
    console.log('Hello UserService Provider');
  }

  getUsers(){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/user/")
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  getUser(id){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/user/" + id)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  updateUser(data) {
    return this.authHttp
      .put(AppSettings.API_ENDPOINT + "/user/"+ data._id, data)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  createUser(data) {
    return this.authHttp
      .post(AppSettings.API_ENDPOINT + "/user/", data)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  deleteUser(id) {
    return this.authHttp
      .delete(AppSettings.API_ENDPOINT + "/user/" + id)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }


}
