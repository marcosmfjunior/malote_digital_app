import { Storage } from '@ionic/storage';
import { AppSettings } from './../app/appSetings';
import { LoadingController } from 'ionic-angular';
import { AuthHttp } from 'angular2-jwt';
import { FileTransferObject, FileTransfer } from '@ionic-native/file-transfer';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Ng2ImgMaxService } from 'ng2-img-max';

declare var window:any ;

/*
  Generated class for the UpServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UpServiceProvider {

  constructor(public http: Http,public ng2ImgMaxService:Ng2ImgMaxService ,private authHttp: AuthHttp,public storage:Storage, public loadingCtrl:LoadingController,private transfer: FileTransfer) {
    console.log('Hello UpServiceProvider Provider');
  }

  uplodFile(file){
    console.log(file)
    return new Promise(resolve => {
    const fileTransfer: FileTransferObject = this.transfer.create();
    // this.ng2ImgMaxService.compressImage(file,0.3,false,false).subscribe(newImg =>{
      this.storage.get('token').then(res => {
        let token = res;
        let url = AppSettings.API_ENDPOINT + "/upload/";

        var options = {
            fileKey: "image",
            fileName: 'fileName.jpeg',
            chunkedMode: false,
            mimeType: "multipart/form-data",
            httpMethod:'POST',
            headers:{
              Authorization: token,
              'x-access-token':token
            }
        };

          fileTransfer.upload(file,url, options)
          .then((results) => {
            let res = JSON.parse(results.response);
            resolve(res)
            console.log(res);
          }, error => {
            console.log(error)
              alert('server error');
          });
        })
      // })
    })

  }

  uploadFilePC(files) {
    return new Promise((resolve, reject) => {
    // this.ng2ImgMaxService.compressImage(files,0.1,true,false).subscribe(
    this.ng2ImgMaxService.resizeImage(files,900,900,false).subscribe(
      result => {
        console.log(result)
        this.storage.get('token').then(res => {
          var formData = new FormData();
          var xhr = new XMLHttpRequest();
          // formData.append("image",)
          formData.append("image", result);


          xhr.onreadystatechange = function () {
              if (xhr.readyState == 4) {
                  if (xhr.status == 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }

          xhr.open("POST", AppSettings.API_ENDPOINT + "/upload/",true);
          xhr.setRequestHeader("Authorization", res);
          xhr.setRequestHeader("x-access-token", res);
          xhr.overrideMimeType("multipart/form-data")
          xhr.send(formData);

        })
      },
      error => {
        console.log('😢 Oh no!', error);
        this.storage.get('token').then(res => {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            // formData.append("image",)
            formData.append("image", files);


            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }

            xhr.open("POST", AppSettings.API_ENDPOINT + "/upload/",true);
            xhr.setRequestHeader("Authorization", res);
            xhr.setRequestHeader("x-access-token", res);
            xhr.overrideMimeType("multipart/form-data")
            xhr.send(formData);

          })
      }
    );



    });
  }
  uploadAudio(file){
    console.log(file);
    return new Promise(resolve => {
    const fileTransfer: FileTransferObject = this.transfer.create();

    this.storage.get('token').then(res => {
      let token = res;
      let url = AppSettings.API_ENDPOINT + "/upload/audio/";

      var options = {
          fileKey: "file",
          fileName: 'my_file.mp3',
          chunkedMode: false,
          mimeType: "audio/mp3",
          httpMethod:'POST',
          headers:{
            Authorization: token,
            'x-access-token':token
          }
      };

        fileTransfer.upload(file, url, options)
        .then((results) => {
          let res = JSON.parse(results.response);
          resolve(res)
          console.log(res);
        }, error => {
          console.log(error)
            alert('server error');
        });
      })
    })
  }
}
