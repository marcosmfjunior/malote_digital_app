import { NavController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { AppSettings } from '../../app/appSetings';
import { Http } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

/*
  Generated class for the DfAdminProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DfAdminService {
  public data: any;
  private dataStore: {
    dfs: any[]
  };
  public loading:any;
  public url = AppSettings.API_ENDPOINT+'/order/';

  constructor(public http: Http,private authHttp: AuthHttp) {
  }

  getDf(){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/root/order")
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  getDfDetail(id){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/root/order/"+id)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  getAnalytics(){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/root/order/analytics/all")
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

}
