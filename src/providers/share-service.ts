import { JwtHelper } from 'angular2-jwt';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { Events, Platform } from 'ionic-angular';
import { User, Order, Address } from './../models/models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export class ShareService {
  admin:string;
  user:User;
  token:string;

  public events:Events = new Events();
  public storage= new Storage(null);
  private jwtHelper = new JwtHelper();
  private platform = new Platform();
  public isCordova = false;

  constructor() {
      this.token = '';
    }

    setCordova(param){
      this.isCordova  = param;
    }
    getCordova(param){
      return this.isCordova;
    }

    setToken(token){
      this.storage.set('token',token).then(res => {
        console.log(res);
        // let user = this.jwtHelper.decodeToken(token);
        // this.setDecodedToken(user);
      })
    }

    setDecodedToken(decodedToken){
      console.log(decodedToken)
      
      // this.events.publish('user:created', this.user);
      // this.setUser(this.user);
    }

    setUser(user) {
      this.user = user;
    }
    
    getUser() {
      return this.user;
    }

    getToken(){
      return this.token;
    }
    
}
