import { AppSettings } from './../app/appSetings';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the ProposalServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PaymentServiceProvider {

  constructor( private authHttp:AuthHttp) {
    console.log('Hello PaymentServiceProvider Provider');
  }

  sendPayment(data) {
    return this.authHttp
      .post(AppSettings.API_ENDPOINT + "/payment/v2/", data)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  editPayment(id,data) {
    return this.authHttp
      .put(AppSettings.API_ENDPOINT + "/payment/v2/"+id,data)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  getPayments(page){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/payment/v2/outstanding/?page=" + page)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  getPaymentsAccepted(searchParams){
    var search = "?page=" + searchParams.page;
    
    if (searchParams.start){
      search += "&start=" + searchParams.start;
    }
    if (searchParams.end){
      search += "&end=" + searchParams.end;
    }

    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/payment/accepted/" + search)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  getPayment(paymentId){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/payment/v2/byid/" + paymentId)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  // accetpProposal(id,data){
  //   return this.authHttp
  //   .post(AppSettings.API_ENDPOINT + "/order/accept-proposal/"+id,data)
  //   .toPromise()
  //   .then(res => res.json(), err => console.log(err));
  // }
  // updateProposal(id,data){
  //   return this.authHttp
  //   .put(AppSettings.API_ENDPOINT + "/proposal/"+id,data)
  //   .toPromise()
  //   .then(res => res.json(), err => console.log(err));
  // }

  // denyProposal(id,data){
  //   return this.authHttp
  //   .post(AppSettings.API_ENDPOINT + "/order/deny-proposal/"+id,data)
  //   .toPromise()
  //   .then(res => res.json(), err => console.log(err));
  // }

  // setInSeparation(id){
  //   return this.authHttp
  //   .get(AppSettings.API_ENDPOINT + "/proposal/set-in-separation/"+ id)
  //   .toPromise()
  //   .then(res => res.json(), err => console.log(err));
  // }

  // setOutForDelivery(id){
  //   return this.authHttp
  //   .get(AppSettings.API_ENDPOINT + "/proposal/set-out-for-delivery/"+ id)
  //   .toPromise()
  //   .then(res => res.json(), err => console.log(err));
  // }

  // setDelivered(id){
  //   return this.authHttp
  //   .get(AppSettings.API_ENDPOINT + "/proposal/set-delivered/"+ id)
  //   .toPromise()
  //   .then(res => res.json(), err => console.log(err));
  // }
}
