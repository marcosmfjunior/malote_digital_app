import { CropAndGetAuthNumberPage } from './../pages/crop-and-get-auth-number/crop-and-get-auth-number';
import { MoneyPipe } from './pipes/moneyPipe';
import { StatusPipe } from './pipes/StatusPipe';
import { ImagePickerComponent } from './../components/image-picker/image-picker';
import { NotesPopOver } from './../pages/notes-pop-over/notes-pop-over';
import { MakePayment_2Page } from './../pages/make-payment-2/make-payment-2';
import { MakePaymentPage } from './../pages/make-payment/make-payment';
import { ProductHistoryComponent } from './../components/product-history/product-history';
import { ProductPickerComponent } from './../components/product-picker/product-picker';
import { ProductsService } from './../providers/products-service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { OrderFilterPopOver } from './../components/order-filter-pop-over/order-filter-pop-over';
import { PopoverOptions } from './../components/order-options-pop-over/order-options-pop-over';
import { OrderStatus } from './pipes/orderStatus';
import { AddressFormat } from './pipes/addressFormat';
import { AdditionalPicker } from './../components/additional-picker/additional-picker';
import { ParallaxHeader } from './../components/parallax-header/parallax-header';
import { AddressPage } from './../components/address/address';

import { CroppieModule } from 'angular-croppie-module';

import { CalendarModule } from "ion2-calendar";

import { Ng2ImgMaxModule } from 'ng2-img-max';

import { TargetTypePipe } from './pipes/target-type';
import { SafeHtml } from './pipes/safe-html';
import { ImagePicker } from '@ionic-native/image-picker';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Geolocation } from '@ionic-native/geolocation';
import { retirePipe } from './pipes/retire-target-gift';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonRating } from './../components/ion-rating/ion-rating';
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { AppSettings } from './appSetings';
import { ShareService } from './../providers/share-service';
import { AuthService } from './../providers/auth-service';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { CurrencyPipe, DecimalPipe} from '@angular/common';
import { Elastic} from './../components/elastic-textarea/elastic-textarea';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { ChartsModule } from 'ng2-charts';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from './../pages/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Facebook } from '@ionic-native/facebook';
import { OneSignal } from '@ionic-native/onesignal';

import { AuthHttp, AuthConfig, JwtHelper } from 'angular2-jwt';
import { Storage, IonicStorageModule} from '@ionic/storage';
import { MomentModule } from 'angular2-moment';

import { Media } from '@ionic-native/media';
import { BrMaskerModule,BrMaskerIonic3 } from 'brmasker-ionic-3';
//import { LongPressModule } from 'ionic-long-press';

import * as ionicGalleryModal from 'ionic-gallery-modal';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { NgxQRCodeModule } from 'ngx-qrcode2';

import { FileOpener } from '@ionic-native/file-opener';
import { Uploader }      from 'angular2-http-file-upload';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { UpServiceProvider } from '../providers/up-service';
import { PaymentServiceProvider } from '../providers/payment-service';
import { PaymentListPage } from '../pages/payments/payment-list';
import { CaptalizeFirstLetter } from './pipes/captalizeFirstLetter';
import { CurrencyFormat } from './pipes/currencyFormat';
import { PaymentDetailPage } from '../pages/payments/payment-detail';




export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({noJwtError: true}), http, options);
}

export function getAuthHttp(http, storage,auth) {
  return new AuthHttp(new AuthConfig({
    noJwtError: true,
    globalHeaders: [{'Content-Type': 'application/json'}],
    tokenGetter: (() => storage.get('token').then((token: string) => token)),
    noTokenScheme: true
  }), http);
}


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    IonRating,
    retirePipe,
    ParallaxHeader,
    AddressPage,
    AdditionalPicker,
    ProductPickerComponent,
    ImagePickerComponent,
    ProductHistoryComponent,
    Elastic,
    OrderStatus,
    CurrencyFormat,
    CaptalizeFirstLetter,
    AddressFormat,
    StatusPipe,
    MoneyPipe,
    PopoverOptions,
    OrderFilterPopOver,
    TargetTypePipe,
    SafeHtml,
    MakePaymentPage,
    MakePayment_2Page,
    NotesPopOver,
    PaymentListPage,
    PaymentDetailPage,
    CropAndGetAuthNumberPage
  ],
  imports: [
    BrowserModule,
    CroppieModule,
    Ng2ImgMaxModule,
    FilterPipeModule,
    HttpModule,
    BrowserAnimationsModule,
    IonicStorageModule.forRoot(),
    BrMaskerModule,
    CalendarModule,
    MomentModule,
    SelectSearchableModule,
    //LongPressModule,
    IonicModule.forRoot(MyApp,{
      backButtonText: '',
    }),
    ionicGalleryModal.GalleryModalModule,
    NgxQRCodeModule,
    ChartsModule,
    // IonicImageLoader.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MakePaymentPage,
    MakePayment_2Page,
    NotesPopOver,
    PaymentListPage,
    CropAndGetAuthNumberPage,
    PaymentDetailPage,
    LoginPage,
    AddressPage,
    ProductHistoryComponent,
    PopoverOptions,
    OrderFilterPopOver
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'pt-BR'},
    {
      provide: AuthHttp,
      useFactory: getAuthHttp,
      deps: [Http,Storage,AuthService]
    },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: ionicGalleryModal.GalleryModalHammerConfig,
    },
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    DecimalPipe,
    Facebook,
    AuthService,
    ShareService,
    UpServiceProvider,
    PaymentServiceProvider,
    OneSignal,
    JwtHelper,
    ImagePicker,
    ProductsService,
    Geolocation,
    FileTransfer,
    FileOpener,
    Camera,
    Media,
    // MediaCapture,
    SafeHtml,
    retirePipe,
    TargetTypePipe,
    OrderStatus,
    CurrencyFormat,
    CaptalizeFirstLetter,
    AddressFormat,
    StatusPipe,
    MoneyPipe,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Uploader
  ]
})
export class AppModule {}
