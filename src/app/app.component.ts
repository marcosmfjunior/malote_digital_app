import { CropAndGetAuthNumberPage } from './../pages/crop-and-get-auth-number/crop-and-get-auth-number';
// import { ImageLoaderConfig } from 'ionic-image-loader';
import { HomePage } from './../pages/home/home';
import { MakePayment_2Page } from './../pages/make-payment-2/make-payment-2';
import { MakePaymentPage } from './../pages/make-payment/make-payment';
import { JwtHelper } from 'angular2-jwt';
import { Storage } from '@ionic/storage';
import { User } from './../models/models';
import { ShareService } from './../providers/share-service';
import { OneSignal } from '@ionic-native/onesignal';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events,App,IonicApp } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from './../pages/login/login';

import {} from '@types/googlemaps';
import { PaymentListPage } from '../pages/payments/payment-list';

declare var google;
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any ;
  user: User;

  pages: Array<{title: string, component: any, permission:number}>;

  constructor(public platform: Platform, private _ionicApp: IonicApp,private _app: App,private events:Events, private storage:Storage, private jwtHelper:JwtHelper, private shareSvc:ShareService, public statusBar: StatusBar, public splashScreen: SplashScreen,private oneSignal:OneSignal) {


    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, permission: 1 },
      { title: 'Home', component: MakePaymentPage, permission: 1 },
      { title: 'Home', component: PaymentListPage, permission: 1 }
    ];

    // this.user = this.shareSvc.getUser();
    // console.log(this.user)
    events.subscribe('user:created', (user) => {
      console.log(user)
      this.user = user;
    });
    this.setupBackButtonBehavior ();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // this.statusBar.styleDefault();
      // let status bar overlay webview
      // this.statusBar.overlaysWebView(true);

      // set status bar to white
      this.statusBar.backgroundColorByHexString('#0c3860');
      this.splashScreen.hide();
      console.log('en')


      // OneSignal Code start:
      // Enable to debug issues:
      // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});


      if (this.platform.is('cordova')){
        this.shareSvc.setCordova(this.platform.is('cordova'));
      }

      //this.nav.setRoot(NewAccountPage);
      this.splashScreen.hide();
      this.checkPreviousAuthorization();
       // disable spinners by default, you can add [spinner]="true" to a specific component instance later on to override this
    //   this.imageLoaderConfig.enableSpinner(false);

    //  // set the maximum concurrent connections to 10
    //   this.imageLoaderConfig.setConcurrency(10);


    });
  }

  checkPreviousAuthorization(): void{
    console.log('entroi')
    this.storage.get("token").then(token => {

      if(token != undefined || token != null){
        let info = this.jwtHelper.decodeToken(token);

        this.shareSvc.setDecodedToken(info);
        this.rootPage = HomePage;
        this.events.publish('user:created',info.user);

        // this.shareSvc.setUser(user);
        this.user = info.user;
          // this.menuCtrl.open();
      }else{
        this.rootPage = LoginPage;
      }
    })
  }

  logout(){
    this.storage.clear().then(res =>{
      console.log(res);
      location.reload();
      this.nav.setRoot(LoginPage);
    });
  }
  openPage(page) {
    this.nav.push(page.component);
  }
  private setupBackButtonBehavior () {

    // If on web version (browser)
    if (window.location.protocol !== "file:") {

      // Register browser back button action(s)
      window.onpopstate = (evt) => {

        // Close any active modals or overlays
        let activePortal = this._ionicApp._loadingPortal.getActive() ||
          this._ionicApp._modalPortal.getActive() ||
          this._ionicApp._toastPortal.getActive() ||
          this._ionicApp._overlayPortal.getActive();

        if (activePortal) {
          activePortal.dismiss();
          return;
        }

        // Navigate back
        if (this._app.getRootNav().canGoBack())
          this._app.getRootNav().pop();

      };

      // Fake browser history on each view enter
      this._app.viewDidEnter.subscribe((app) => {
        history.pushState (null, null, "");
      });

    }

  }

}
