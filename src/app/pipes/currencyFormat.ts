import {Pipe} from '@angular/core';

@Pipe({
 name: 'CurrencyFormat'
})
export class CurrencyFormat {
  transform(value) {
    if (value != undefined){
      var s = ".";
      var c = ",";
  
      var re = '\\d(?=(\\d{' + (3) + '})+' + (2 > 0 ? '\\D' : '$') + ')',
      num = value.toFixed(Math.max(0, ~~2));
      
      var ret = (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));

      return "R$ " + ret;
    }
    else{
      return "";
    }
  }
}


