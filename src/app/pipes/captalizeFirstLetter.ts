import {Pipe} from '@angular/core';

@Pipe({
 name: 'CaptalizeFirstLetter'
})
export class CaptalizeFirstLetter {
  transform(value) {
    if (value != undefined){
      return value.replace(/^\w/, c => c.toUpperCase());
    }
    else{
      return "";
    }
  }
}


