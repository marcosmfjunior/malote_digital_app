import {Pipe} from '@angular/core';

@Pipe({
 name: 'retirePipe'
})

export class retirePipe {
  transform(days) {
    if(days == null || days==undefined)
      return null;
    let weekday = ["DOM","SEG","TER","QUA","QUI","SEX","SAB"];
    let diasDisponiveis = '';
    days.sort(function(a, b){return b-a});
    for(var tam=0, i = days.length; i>tam;i--){
      // console.log(weekday[dias[i]]);
      diasDisponiveis= weekday[days[i-1]] +' '+ diasDisponiveis;
    }
    return diasDisponiveis;
}
}
