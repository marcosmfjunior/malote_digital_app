import {Pipe} from '@angular/core';

@Pipe({
 name: 'OrderStatus'
})
export class OrderStatus {
  transform(check) {
    if(check == 'active')
      return "Em aberto"
    else if(check == 'finished')
      return 'Finalizado'
    else if(check == 'canceled')
      return 'Cancelado'
    else if(check == 'partially_active')
      return 'Parcialmente Ativa'
    else if(check == 'done_deal')
      return 'Negócio Fechado'
    else
      return check;
}
}


