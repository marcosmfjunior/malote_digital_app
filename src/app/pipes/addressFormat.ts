import { Pipe } from '@angular/core';

@Pipe({
  name: 'addressFormat'
})
export class AddressFormat {
  transform(address) {
    console.log(address);
    if (address){
      var addressFormated = address.name + ", " + address.num + " - " + address.neighborhood +
      ", " + address.city + " - " + address.state + ", " + address.postal_code + ", Brasil";
  
      return addressFormated;
    }
    else{
      return "Não informado.";
    }
  }
}


