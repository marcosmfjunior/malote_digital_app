import {Pipe} from '@angular/core';

@Pipe({
 name: 'statusPipe'
})
export class StatusPipe {
  transform(check) {
    if(check == 'sent')
      return "Enviada"
    else if(check == 'rejected')
      return 'Rejeitada'
    else if(check == 'resent')
      return 'Reenviada'
    else if(check == 'accepted')
      return 'Aceita'
  }
}


