import { CurrencyFormat } from './currencyFormat';
import {Pipe} from '@angular/core';

@Pipe({
 name: 'moneyPipe'
})
export class MoneyPipe {
    constructor(public currencyPipe: CurrencyFormat) {
  }
  transform(value) {
    let newValue = Object.assign(value);
    if(typeof value == 'number')
      return this.currencyPipe.transform(value);
    else
      return 'R$'+newValue
  }
}


