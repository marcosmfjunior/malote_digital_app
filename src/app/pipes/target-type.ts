import {Pipe} from '@angular/core';

@Pipe({
 name: 'targetTypePipe'
})
export class TargetTypePipe {
  transform(type) {
    if(type == 'checkout')
      return "leituras de QrCode"
    else if(type == 'spend')
      return 'reais gastos'
    else if(type == 'liters')
      return 'combustível comprados'
}
}


