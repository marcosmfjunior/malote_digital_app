import {HostListener, Directive} from '@angular/core';

/*
  Generated class for the ElasticTextarea directive.

  See https://angular.io/docs/ts/latest/api/core/index/DirectiveMetadata-class.html
  for more info on Angular 2 Directives.
*/
@Directive({
    selector: '[elastic]'
})
export class Elastic {
    @HostListener('input',['$event.target'])
    onInput(nativeElement: any): void {
        nativeElement.style.height = nativeElement.scrollHeight + "px";
    }
}
