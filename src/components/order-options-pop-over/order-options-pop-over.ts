import { ShareService } from './../../providers/share-service';
import { AlertController } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { ViewController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'order-options-pop-over',
  template: `
  <ion-list>
    <button ion-item (click)="edit()">Editar</button>
    <button ion-item (click)="remove()">Remover</button>
  </ion-list>
  `
})
export class PopoverOptions {
  public product;
  public index;

  constructor(public viewCtrl:ViewController, public navParams:NavParams, public navCtrl:NavController, public alertCtrl:AlertController, public shareSvc:ShareService) {
    console.log('Hello OrderOptionsPopOver Component');
    this.product = this.navParams.get('item');
    this.index = this.navParams.get('index');
  }

    close() {
      this.viewCtrl.dismiss();
    }

    edit(){
      // this.navCtrl.push(ClassesDetailOrder,{item:this.product,classe:this.product.menu.class,index:this.index,edit:true});
      this.viewCtrl.dismiss();
    }

    remove(){
      let alert = this.alertCtrl.create({
        title: 'Confirma remoção',
        message: 'Você deseja remover '+this.product.menu.name+' ?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
              this.viewCtrl.dismiss();
            }
          },
          {
            text: 'Remover',
            handler: () => {
              this.viewCtrl.dismiss();
            }
          }
        ]
      });
      alert.present();


      console.log(this.product)
    }

}
