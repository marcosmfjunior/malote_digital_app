import { Observable } from 'rxjs/Observable';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { EmailValidator, PasswordValidation } from './../../app/validators';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';
import { default as cep } from 'cep-promise';

declare var google;

@Component({
  selector: 'page-address',
  templateUrl: 'address.html',
})
export class AddressPage {
  public address;
  public form;
  public submitAttempt: boolean = false;

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  geocoder: any;
  marker: any;

  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    private formBuilder: FormBuilder) {

      this.address = this.navParams.data.address || {};

      this.form = this.formBuilder.group({
        name: ["", Validators.required],
        num: ['', [Validators.required]],
        neighborhood: ['', [Validators.required]],
        complement: [''],
        postal_code: ['', [Validators.required]],
        state: ['', [Validators.required]],
        city: ['', [Validators.required]],
        location: this.formBuilder.group({
          type: ['Point'],
          coordinates: this.formBuilder.array([
  
          ]),
        })
      });

      if (this.address) {
        
        this.form.controls['name'].setValue(this.address.name);
        this.form.controls['num'].setValue(this.address.num);
        this.form.controls['neighborhood'].setValue(this.address.neighborhood);
        this.form.controls['complement'].setValue(this.address.complement);
        this.form.controls['postal_code'].setValue(this.address.postal_code);
        this.form.controls['city'].setValue(this.address.city);
        this.form.controls['state'].setValue(this.address.state);
        if (this.address.location) {
          this.form.value.location.coordinates = this.address.location.coordinates || [];
        }
        else {
          this.address.location = {
            type: "Point",
            coordinates: []
          }
          this.form.value.location = this.address.location;
        }
  
      }
  }

  ionViewDidLoad() {
    
    var that = this;
    setTimeout(function(){
      that.loadMap();
    }, 1000);
  }

  loadMap() {
    let latLng = new google.maps.LatLng(-32.1074854, -52.147094);

    console.log(this.address);
    if (this.address.location.coordinates != undefined && this.address.location.coordinates.length > 0) {
      latLng = new google.maps.LatLng(this.address.location.coordinates[1], this.address.location.coordinates[0]);
    }

    this.geocoder = new google.maps.Geocoder;

    let mapOptions = {
      center: latLng,
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    this.marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latLng
    });

    var that = this;

    google.maps.event.addListener(this.map, 'click', function(event) {
      that.marker.setPosition(event.latLng);

      that.form.value.location.coordinates = [
        event.latLng.lng(), event.latLng.lat()
      ];
    });

    var input = document.getElementById('pac-input'); 
    var searchBox = new google.maps.places.SearchBox(input);
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    this.map.addListener('bounds_changed', function() {
      searchBox.setBounds(that.map.getBounds());
    });

    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      that.map.fitBounds(bounds);
    });

    // Add a marker at the center of the map.
    // addMarker(bangalore, map);
  }

  searchCEP(postalCode) {
    var formThat = this.form;
    cep(postalCode).then(function (result) {
      if (result) {
        formThat.controls['name'].setValue(result.street);
        formThat.controls['num'].setValue("");
        formThat.controls['neighborhood'].setValue(result.neighborhood);
        formThat.controls['complement'].setValue("");
        formThat.controls['city'].setValue(result.city);
        formThat.controls['state'].setValue(result.state);
        formThat.value.location.coordinates = [];
      }
    })
  }

  updateMap(address) {
    // if (address.name != "" && address.num != "" && address.neighborhood != "" &&
    //   address.city != "" && address.state != "" && address.postal_code) {
    //   var addressFormated = address.name + ", " + address.num + " - " + address.neighborhood +
    //     ", " + address.city + " - " + address.state + ", " + address.postal_code + ", Brasil";

    //   var that = this;
    //   this.geocoder.geocode({ 'address': addressFormated }, function (results, status) {

    //     if (status === google.maps.GeocoderStatus.OK) {
    //       if (results[0]) {

    //         var latLng = {
    //           lng: results[0].geometry.location.lng(),
    //           lat: results[0].geometry.location.lat()
    //         }

    //         that.marker.setPosition(latLng);
    //         that.map.setCenter(that.marker.getPosition());

    //         that.form.value.location.coordinates = [
    //           results[0].geometry.location.lng(), results[0].geometry.location.lat()
    //         ];

    //       }
    //     }
    //   });
    // }

  }

  save(value: any) {
    console.log(value);
    if (this.form.valid) {

      Object.assign(this.address, value);
      var callbackConfirm = this.navParams.get('onConfirm');
      callbackConfirm(this.address);
      this.navCtrl.pop();
    }
    else {
      this.submitAttempt = true;
    }
  }
}
