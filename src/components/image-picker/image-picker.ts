import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { UpServiceProvider } from './../../providers/up-service';
import { Platform, LoadingController, ModalController } from 'ionic-angular';
import { Component, Input } from '@angular/core';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { GalleryModal } from 'ionic-gallery-modal';

@Component({
  selector: 'image-picker',
  templateUrl: 'image-picker.html'
})


export class ImagePickerComponent {
  private _imageArray;
  public picture;
  @Input()
  set imageArray(imageArray) {
    console.log(this._imageArray)
    this._imageArray = new Array();
    console.log(this._imageArray)
    console.log(imageArray)
    this._imageArray = imageArray;
    // this.getProducts(typeProduct);
  }

  text: string;

  constructor(
    public platform:Platform,
    public loadingCtrl:LoadingController,
    public upService:UpServiceProvider,
    public actionSheetCtrl:ActionSheetController,
    public camera:Camera,
    public modalCtrl:ModalController
  ) {
    console.log('Hello ImagePickerComponent Component');
    this.text = 'Hello World';
  }
  addMedia(){
    if(this.platform.is('core') || this.platform.is('mobileweb')) {
      var fileElem = document.getElementById("filePicker");
      fileElem.click();
    }else if(this.platform.is('ios') || this.platform.is('android')){
      this.showActionSheet();
      // this.getNewImage(2);
      // console.log(this.platform)
        console.log('app')
    }else
      alert('erro')
  }
  viewPic(indexPic){
    let modal = this.modalCtrl.create(GalleryModal, {
      photos: this.picture,
      initialSlide: indexPic
    });
    modal.present();
  }
handleFileSelect(evt){

  var files = evt.target.files;
  var file = files[0];
  let loading = this.loadingCtrl.create({
    content: 'Enviando mídia'
  });

  loading.present();
  this.upService.uploadFilePC(file).then(res => {
    let response : any = res;
    loading.dismiss();
    this._imageArray.push(response.url)
    console.log(this._imageArray)
  })
}
showActionSheet(){
  let actionSheet = this.actionSheetCtrl.create({
    title: "Selecione: ",
    buttons: [
      {
        text: 'Tirar foto',
        handler: () => {
          console.log('tirar foto')
          this.getImage(1);
        }
      },{
        text: 'Utilizar galeria',
        handler: () => {
          console.log('galeria')
          this.getImage(2)
        }
      }
    ]
  });
  actionSheet.present();
}
getImage(mode){
  console.log(mode)
  // return mode;

  var sourceType = mode;
    const options: CameraOptions = {
        quality: 60,
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: sourceType,
        allowEdit: false,
        encodingType: this.camera.EncodingType.JPEG,
        saveToPhotoAlbum: false,
        correctOrientation:true,
        targetWidth: 800,
    };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.upService.uplodFile(base64Image).then(response =>{
        console.log(response)
        let res:any = response;
        this._imageArray.push(res.url)
        // if(res.success){
        //   this.events.publish("imageUploaded",res.url,page);
        // }

          // console.log('oi')
          // this.viewCtrl.dismiss();
      })
      }, (err) => {
      // Handle error
      });
}

}
