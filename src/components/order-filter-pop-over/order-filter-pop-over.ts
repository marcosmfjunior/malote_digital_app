import { ShareService } from './../../providers/share-service';
import { AlertController } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { ViewController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'order-filter-pop-over',
  template: `<ion-list radio-group class="popover-page" [(ngModel)]="params" (ionChange)="changed()">
  <ion-list-header>
    Filtrar Pedidos
  </ion-list-header>

    <ion-item class="text-athelas">
      <ion-label>Em aberto</ion-label>
      <ion-radio checked value="openOrders"></ion-radio>
    </ion-item>

    <ion-item class="text-athelas">
      <ion-label>Encerrados</ion-label>
      <ion-radio value="closedOrders"></ion-radio>
    </ion-item>

    </ion-list>`
})
export class OrderFilterPopOver {
  public params;

  constructor(public viewCtrl:ViewController, public navParams:NavParams, public navCtrl:NavController, public alertCtrl:AlertController, public shareSvc:ShareService) {
    console.log('Hello OrderOptionsPopOver Component');
    this.params = this.navParams.get('params');

  }

  changed(){
    if(this.params != undefined){
      console.log(this.params);
      this.viewCtrl.dismiss(this.params);
    }
  }



}
