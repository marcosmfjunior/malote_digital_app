import { Additional } from './../../models/models';
import { AdditionalService } from './../../providers/additional-service';
import { Storage } from '@ionic/storage';
import { ShareService} from './../../providers/share-service';

import { Observable } from 'rxjs/Observable';
import { Component,Input,Output,EventEmitter } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';

@Component({
  selector: 'additional-picker',
  templateUrl: 'additional-picker.html'
})

export class AdditionalPicker {

  public addtionals: any;
  public addtionals_: Observable<Additional[]>;
  public _name:string;
  public erroMessage;
  public _multiple;
  public _sharedVar:any[];
  public _withGeral:boolean;
  public options;
  public lala;
  public _item;
  public _edit;
  constructor(public navCtrl: NavController,public storage:Storage, public events:Events,public navParams: NavParams, public additionalSvc:AdditionalService) {
    // this.additionalSvc.getAdditionals();
  }

  @Input()
  set item(item: any) {
    console.log(item)
    this._item = item;
  }


  @Input()
  set edit(edit: boolean) {
    this._edit = edit;
  }

  @Input()
  set sharedVar(sharedVar: any) {
    console.log(sharedVar)
    this._sharedVar = sharedVar;
  }

  @Output() sharedVarChange = new EventEmitter();
  @Output() priceChange = new EventEmitter();


  ngOnInit() {
    console.log(this._edit)
    if(this._edit && this._sharedVar.length > 0)
      this.addtionals = this._sharedVar;
    else{
      this.additionalSvc.getAdditionals().then(res =>{
        this.addtionals = res;
        this.sharedVarChange.emit(this.addtionals);
      })
    }
    // this.addtionals = this.additionalSvc.items; // subscribe to entire collection
    // this.additionalSvc.getAdditionals();    // load all todos*/
  }

  emitChange(){
    // this.sharedVar = newValue;
    this.events.publish(this._item._id+'Changed', true);
    this.sharedVarChange.emit(this.addtionals);
    this.priceChange.emit(this._item.price);
  }
  addAdditional(item){
    if( item.amount < 3){
      this._item.price += item.price;
      this.emitChange();
      return item.amount ++;
    }
  }

  removeAdditional(item){
    if(item.amount != 0 && item.amount > 0){
      this._item.price -= item.price;
      this.emitChange();
      return item.amount --;
    }
  }

}
