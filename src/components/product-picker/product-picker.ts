import { ProductHistoryComponent } from './../product-history/product-history';
import { Product } from './../../models/models';
import { ProductsService } from './../../providers/products-service';
import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Subscription } from 'rxjs';
import { ArrayBuffer } from '@angular/http/src/static_request';
import { Events, ModalController } from 'ionic-angular';

@Component({
  selector: 'product-picker',
  templateUrl: 'product-picker.html'
})

export class ProductPickerComponent {
  products: any;
  countries: any[];
  product: Product;
  portForm: FormGroup;
  portNameControl: FormControl;
  portCountryControl: FormControl;
  selectedItem:any;
  showHistory = false;
  private _typeProduct: string;
  idProductSelected;

  @Output() textValue = new EventEmitter<any>();

  @Output() nameChanged = new EventEmitter();

  @ViewChild('portComponent') portComponent: SelectSearchableComponent;
  // @Input('typeProduct') typeProduct: any;

  @Input()
  set typeProduct(typeProduct: string) {
    this._typeProduct = typeProduct;
    this.getProducts(typeProduct);
  }

  constructor(
    private productsService: ProductsService,
    private formBuilder: FormBuilder,
    public events:Events,
    public produtcsSvc:ProductsService,
    public modalCtrl:ModalController
  ) {
    var that = this;

    // setTimeout(function(){
    //   console.log(that.typeProduct);

    //   productsService.getProductsDB(that.typeProduct).then( res => {
    //     that.products = res;
    //     // that.portComponent.endSearch();
    //   })
    // }, 500);

    // this.countries = this.portService.getCountries();

    // Create port form that will be used to add or save port.
    this.portNameControl = this.formBuilder.control(null, Validators.required);
    this.portCountryControl = this.formBuilder.control(null, Validators.required);
    this.portForm = this.formBuilder.group({
      portName: this.portNameControl,
      portCountry: this.portCountryControl
    });

  }

  getProducts(type){
      this.productsService.getProductsDB(type).then( res => {
        this.products = res;
        // that.portComponent.endSearch();
      })
  }

  portChange(event: {
    component: SelectSearchableComponent,
    value: any
  }) {
    console.log('port:', event.value);
    this.events.publish("produtChanged",event.value)
    this.getHistory(event.value._id)

  }
  getHistory(id){
    this.idProductSelected = id;
    this.produtcsSvc.getHistory(id).then(res =>{
      console.log(res)
      this.showHistory = true;
      // this.setChart(res);
    })
  }
  showHistoryModal(){
    var historyModal = this.modalCtrl.create(ProductHistoryComponent, { idProduct: this.idProductSelected }, {showBackdrop: true});
    historyModal.present();
  }

  onSearchFail(event: {
    component: SelectSearchableComponent,
    text: string
  }) {
    // Clean form.
    this.portNameControl.reset();
    this.portCountryControl.reset();

    // Copy search text to port name field, so
    // user doesn't have to type again.
    this.portNameControl.setValue(event.component.searchText);

    // Show form.
    event.component.showAddItemTemplate();
  }

  onSearchSuccess(event: {
    component: SelectSearchableComponent,
    text: string
  }) {
    // Hide form.
    event.component.hideAddItemTemplate();
  }

  addPort(event) {
    console.log(this.portComponent.searchText)
    console.log(this.portNameControl.value)
    // Create port.
    let port = new Product();
    this.productsService.sendProduct({name:this.portComponent.searchText, type: this._typeProduct}).then(res => {
      console.log(res);

      this.events.publish("produtChanged",res.data)
      // this.products.push(res.data);
      // Add port to the top of list.
      this.products.push(res.data);
      this.selectedItem = res.data;
      this.nameChanged.emit(res.data.name);
      // this.getHistory(res.data._id)

      // this.nameProdutc = res.data.name;
      this.portComponent.close();



      // this.portComponent.addItem(res.data).then(() => {
      //   console.log('add')
      //   // this.portComponent.search(res.data.name);
      //   this.selectedItem = res.data;
      //   // this.portNameControl.reset();
      //   // this.portCountryControl.reset();

      //   if (this.portComponent.isEnabled && this.portComponent.isOpened) {
      //     console.log('entrou')
      //     this.portComponent.close();
      //   }else{
      //     console.log('nao entrou')

      //   }
      //   // this.portComponent.close();
      // });
    })

  }
  onSavePort(event: {
    component: SelectSearchableComponent
  }) {
    console.log('wjjwjwj')
    // Save port.
  }
}
