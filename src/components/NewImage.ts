import { ViewController } from 'ionic-angular/navigation/view-controller';
import { Component } from '@angular/core';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { ToastController, NavController, Events } from 'ionic-angular';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { UpServiceProvider } from '../providers/up-service';

// @Component({
//   selector: 'add-item-to-order'
// })
export class NewImage {

  text: string;
  public edit:boolean = false;
  public editIndex;
  constructor(
    public navCtrl:NavController,
    public toastCtrl:ToastController,
    public actionSheetCtrl:ActionSheetController,
    public camera:Camera,
    public upService:UpServiceProvider,
    public events:Events,
    public viewCtrl:ViewController) {
    console.log('Hello AddItemToOrderComponent Component');
    this.text = 'Hello World';
  }
  getNewImage(page){
    return this.showActionSheet(page);
    // return 'kkkk'
  }
  getImageApp = new Promise((resolve, reject) => {
    // Do some async stuff
    this.showActionSheet(null);
    // If async opp successful
    resolve();

    // If async opp fails
    reject();
  });

  showErrorToast(message){
    const toast = this.toastCtrl.create({
      message: message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      cssClass: "error-toast",
    });
    toast.present();
  }
  showActionSheet(page){
    let actionSheet = this.actionSheetCtrl.create({
      title: "Selecione: ",
      buttons: [
        {
          text: 'Tirar foto',
          handler: () => {
            console.log('tirar foto')
            this.getImage(1,page);
          }
        },{
          text: 'Utilizar galeria',
          handler: () => {
            console.log('galeria')
            this.getImage(2,page)
          }
        }
      ]
    });
    actionSheet.present();
  }
  getImage(mode,page){
    console.log(mode)
    // return mode;

    var sourceType = mode;
      const options: CameraOptions = {
          quality: 60,
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType: sourceType,
          allowEdit: false,
          encodingType: this.camera.EncodingType.JPEG,
          saveToPhotoAlbum: false,
          correctOrientation:true,
          targetWidth: 800,
      };
      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        this.upService.uplodFile(base64Image).then(response =>{
          console.log(response)
          let res:any = response;
          if(res.success){
            this.events.publish("imageUploaded",res.url,page);
          }

            console.log('oi')
            this.viewCtrl.dismiss();
        })
        }, (err) => {
        // Handle error
        });
  }
}
