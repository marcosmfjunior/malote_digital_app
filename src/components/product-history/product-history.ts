import { ViewController } from 'ionic-angular/navigation/view-controller';
import { NavParams } from 'ionic-angular';
import { ProductsService } from './../../providers/products-service';
import { Component, Input } from '@angular/core';

/**
 * Generated class for the ProductHistoryComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'product-history',
  templateUrl: 'product-history.html'
})
export class ProductHistoryComponent {
  public orderPerDayLabel ;

  @Input()
  set idProduct(idProduct: string) {
    this.getHistory(idProduct);
  }
  public lineChartOptions: any = {
    stacked: false,
    legend: {
      display: true,
      position: 'top'
    },
    hover: {
      mode: 'nearest',
      intersect: true
    },
    tooltips: {
      mode: 'index',
      intersect: false,
    },
    maintainAspectRatio: false,
    spanGaps: false,
    elements: {
      line: {
        tension: 0.01
      }
    },
    plugins: {
      filler: {
        propagate: false
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          autoSkip: false,
          maxRotation: 45,
          minRotation: 45
        }
      }]
    },
  };
  text: string;
  // public statusOrdersLabel: string[] = ["Carregando"];
  public statusOrdersData: any[];
  // public statusOrdersData: any[] = [{label:"fornec 1",fill:false,data:[19,10]},{label:"fornec 2",fill:false,data:[99,20]}];
  // public orderPerDayLabel: string[] = ['01/08','02/08','03/08' ];

  constructor( public produtcsSvc:ProductsService, public navParams:NavParams,public viewCtrl:ViewController) {
    console.log('Hello ProductHistoryComponent Component');
    this.text = 'Hello World';
    let idProduct = this.navParams.get('idProduct');
    console.log(idProduct)
    if(idProduct)
      this.getHistory(this.navParams.get('idProduct'))
  }

  getHistory(id){
    this.produtcsSvc.getHistory(id).then(res =>{
      console.log(res)
      this.setChart(res);
    })
  }

  setChart(data){
    console.log(data)
    this.setLabels(data);
    this.setDataCharts(data);
  }

  setDataCharts(datas){
    this.statusOrdersData = new Array();
    this.getProviders(datas);
    this.setInfo(datas);
  }
  setLabels(dates){
    this.orderPerDayLabel = new Array();
    dates.forEach( date => {
      let _date = date._id.day + '/'+ date._id.month + '/'+date._id.year;
      this.orderPerDayLabel.push(_date)
    });
    console.log(this.orderPerDayLabel)
  }
  getProviders(datas){
    datas.forEach(data => {
      data.providers.forEach(provider => {
        if(this.statusOrdersData.find(function (label){if(label.label == provider.name) return true; })){
          console.log('ja tem')
        }else{
          let data = new Array(this.orderPerDayLabel.length+1);
          data.fill(0, 0,this.orderPerDayLabel.length+1);
          console.log(data)
          let providerInfo = {label:provider.name,fill:false,data:data}
          this.statusOrdersData.push(providerInfo);
        }
      });
    });
    return true;
  }
  setInfo(datas){
    datas.forEach((data,i) => {
      data.providers.forEach(provider => {
        let providerInfo = (this.statusOrdersData.find(providerLabel => providerLabel.label === provider.name));
        providerInfo.data[i] = provider.price;
        for(let j = i; j<providerInfo.data.length; j++){
          providerInfo.data[j] = provider.price;
        }
      });
    });
  }

  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'transparent',
      borderColor: 'red',
      pointBackgroundColor: 'red',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'transparent',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'transparent',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
