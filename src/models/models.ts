import { Message } from './models';
export class User {
  address: Address[] = new Array();
  access:number;
  constructor(public name:string, public phone:string, public _id:string){
    this.name = name;
    this.phone = phone;
    this._id = _id;
    this.address = new Array();
  }
}
export class Payment{
  media  = new Array();
  _id:string;
  status:string;
  value:any;
  type: string;
  derivation:string;
  days : DaysSelect[]= new Array();
}
export class DaysSelect{
  day_selected:any;
  notes: Notes[] = new Array();
}
export class Product{}
export class Notes{
  type:string;
  value:any;
  justification:string;
  media:Media[]= new Array();
}
export class Class{
    name: string;
    description: string;
    _id: string;
    image: string;
}
export class Ingredients{
    name:string;
    qtd:number;
    _id:string;
}
export class Menu{
  name: string;
  description: string;
  _id: string;
  image: string;
  class:string;
  off: number;
  price: number;
  ingredients:[Ingredients];
}

export class Additional{
  name: String;
  description: string;
  price:number;
  amount:number = 0;
  _id:string;
}


export class ProductOrder{
  menu: Menu;
  additional:Additional[];
  price:number;
  note:string;
  qtd:number = 1;
}

export class Order{
  address: Address;
  itens: ProductOrder[] = new Array();
  total: number = 0;
  delivery:boolean ;
}

export class Address{
  name:string;
  num:number;
  neighborhood:string;
  complement:string;
}


export enum MessageType {
  TEXT = <any>'text',
  LOCATION = <any>'location',
  PICTURE = <any>'picture',
  AUDIO = <any>'audio'
}

export interface Media {
  content?: string;
  type?: MessageType;
}

export interface Location {
  lat: number;
  lng: number;
  zoom: number;
}

export interface Chat {
  _id?: string;
  title?: string;
  picture?: string;
  lastMessage?: Message;
}

export interface Message {
  _id?: string;
  chatId?: string;
  content?: string;
  createdAt?: Date;
  type?: MessageType;
  ownership?: string;
  sender?:string;
}

export interface Picture {
  _id?: string;
  complete?: boolean;
  extension?: string;
  name?: string;
  progress?: number;
  size?: number;
  store?: string;
  token?: string;
  type?: string;
  uploadedAt?: Date;
  uploading?: boolean;
  url?: string;
  userId?: string;
}

export class Res{
    success:true;
    message:string;
    info:any;
}
export class Mural{
    itens:[{
        title:string;
        description:string;
        show:any[];
        expires:string;
        _id:string;
    }];
    _id:string;
}

export class ContactModel{
    name:string;
    _id:string;
    phone:number;
    address:string;
    kind:string;
    show:any[];
}

export class Sector{
    name:string;
    _id:string;
}

export class Target{
    _id:string;
    image:string;
    type:string;
    val:number;
    title:String;
    description:String;
    validity:Number;
    published:Boolean;
    gift:Gift;
    retire:[any]
}

export class Gift{
  price:number;
  off:number;
  name:String;
}

export class UserInfo{
  checkin:number;
  spend:number;
  checkout:number;
  qtdgasol:number;
}
