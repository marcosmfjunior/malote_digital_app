import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController, Platform, ToastController, Events } from 'ionic-angular';
import { Notes } from '../../models/models';
import { FormBuilder, Validators } from '@angular/forms';
import { UpServiceProvider } from '../../providers/up-service';
import { GalleryModal } from 'ionic-gallery-modal';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NewImage } from '../../components/NewImage';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { BrMaskerIonic3, BrMaskModel } from 'brmasker-ionic-3';

/**
 * Generated class for the NotesPopOverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-notes-pop-over',
  templateUrl: 'notes-pop-over.html',
  providers: [BrMaskerIonic3]

})
export class NotesPopOver {
  public noteForm;
  public submitAttempt: boolean = false;
  public note = new Notes();
  public media = new Array();
  public picture = new Array();
  public itemEdit = null;
  public loadingImg: boolean = false;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public upService: UpServiceProvider,
    public alertCtrl: AlertController,
    public brMaskerIonic3: BrMaskerIonic3,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public actionSheetCtrl: ActionSheetController,
    public camera: Camera,
    public events: Events,
    public viewCtrl: ViewController
  ) {
    // super(navCtrl,toastCtrl,actionSheetCtrl,camera,upService,events,viewCtrl);
    console.log("sjjsjsjsjsjsjjs")
    if (this.navParams.get('edit')) {
      this.itemEdit = JSON.parse(JSON.stringify(this.navParams.get('note')));
      this.media = this.itemEdit.media;
      this.itemEdit.media.forEach(element => {
        this.picture.push({ url: element })
      });
    }

    if (this.itemEdit) {

      this.note = this.itemEdit;

      this.noteForm = formBuilder.group({
        value: [this.itemEdit.value],
        type: [this.itemEdit.type],
        media: [this.itemEdit.media],
        justification: [this.itemEdit.justification]
        // media:[this.itemEdit.media]
      })
      this.note.media = this.itemEdit.media;
      this.media = this.itemEdit.media;
    } else {
      this.media = new Array();
      this.picture = new Array();
      this.noteForm = formBuilder.group({
        value: ["", Validators.required],
        type: ["", Validators.required],
        media: [""],
        justification: ["", Validators.required]
      })
    }

  }
  ionViewWillEnter() {
  }

  formatMoney(value) {
    if (value != undefined) {

      if (typeof value == 'number') {
        value = value + '';
        value = parseFloat(value).toFixed(2);
        value = parseFloat(value.replace(/[\D]+/g, ''));
      }
      else {
        value = value + '';
        value = value.replace(/\./g, '');
        value = value.replace(new RegExp(',', 'g'), '.');
        value = parseFloat(value.replace(/[\D]+/g, ''));
      }

      value = value + '';

      var tmp = value;

      tmp += '';
      tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
      if (tmp.length > 6) {
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
      }
      if (tmp.length > 10) {
        tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g, ".$1.$2,$3");
      }

      return tmp;
    }
    else {
      return value;
    }
  }

  addMedia() {
    if (this.platform.is('core') || this.platform.is('mobileweb')) {
      var fileElem = document.getElementById("imagePicker");
      fileElem.click();
    } else if (this.platform.is('ios') || this.platform.is('android')) {
      this.showActionSheet();

      console.log('app')
    } else
      alert('erro')
  }

  handleFileSelect(evt) {

    var files = evt.target.files;
    var file = files[0];

    this.loadingImg = true;
    let toast = this.toastCtrl.create({
      message: 'Enviando mídia',
      position: 'top'
    });
    toast.present()
    // loading.present();
    if (file) {
      this.upService.uploadFilePC(file).then(res => {
        let response: any = res;
        // loading.dismiss();
        this.loadingImg = false;
        toast.dismiss();

        this.note.media.push(response.url)
      })
    }

  }
  showActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Selecione: ",
      buttons: [
        {
          text: 'Tirar foto',
          handler: () => {
            console.log('tirar foto')
            this.getImage(1);
          }
        }, {
          text: 'Utilizar galeria',
          handler: () => {
            console.log('galeria')
            this.getImage(2)
          }
        }
      ]
    });
    actionSheet.present();
  }
  getImage(mode) {
    console.log(mode)
    // return mode;

    var sourceType = mode;
    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: sourceType,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      targetWidth: 800,
    };
    this.camera.getPicture(options).then((imageData) => {
      this.loadingImg = true;
      let toast = this.toastCtrl.create({
        message: 'Enviando mídia',
        position: 'top'
      });
      toast.present()
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.upService.uplodFile(base64Image).then(response => {
        toast.dismiss();
        this.loadingImg = false;
        console.log(response)
        let res: any = response;
        this.note.media.push(res.url)
        // if(res.success){
        //   this.events.publish("imageUploaded",res.url,page);
        // }

        // console.log('oi')
        // this.viewCtrl.dismiss();
      })
    }, (err) => {
      // Handle error
    });
  }


  deleteMedia(index) {
    console.log(index)
    let alert = this.alertCtrl.create({
      title: 'Confirma remoção',
      message: 'Você deseja remover essa imagem ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Remover',
          handler: () => {

            this.picture.splice(index, 1)
            this.note.media.splice(index, 1)
          }
        }
      ]
    });
    alert.present();
  }
  viewPic(indexPic, arraySelected) {
    let arrayToShow = new Array();
    arraySelected.forEach(pic => {
      arrayToShow.push({ url: pic });
    });
    let modal = this.modalCtrl.create(GalleryModal, {
      photos: arrayToShow,
      initialSlide: indexPic
    });
    modal.present();
  }
  numberOnly(event): boolean {
    console.log(event);
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  checkForm(event, obj) {

    if (!obj.cancel){

      this.noteForm.controls['media'].setValue(this.note.media);

      if (this.noteForm.valid) {
        this.viewCtrl.dismiss(this.note);
        event.preventDefault();
      } else
        this.submitAttempt = true;
    }
    else{
      this.viewCtrl.dismiss();
      event.preventDefault();
    }
  }

  getImageFromCamera() {

  }

  close() {
    this.viewCtrl.dismiss();
  }
  // saveEdit() {
  //   this.viewCtrl.dismiss(this.note);
  // }

}
