import { MoneyPipe } from './../../app/pipes/moneyPipe';
import { PaymentDetailPage } from './../payments/payment-detail';
import { StatusPipe } from './../../app/pipes/StatusPipe';
import { PaymentServiceProvider } from './../../providers/payment-service';
import { MakePaymentPage } from './../make-payment/make-payment';
import { ProductsService } from './../../providers/products-service';
import { ShareService } from './../../providers/share-service';
import { User, UserInfo } from './../../models/models';
import { AuthService } from './../../providers/auth-service';
import { Component,ViewChild, ElementRef } from '@angular/core';
import { NavController, ModalController, Events, AlertController, PopoverController, LoadingController } from 'ionic-angular';
import { PaymentListPage} from '../payments/payment-list';

declare var google;
declare var window;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  user:User;
  public payments = new Array() ;
  public page = 1;
  public totalDocs = 0;

  constructor(public navCtrl: NavController,
              public loadingCtrl:LoadingController,
              public popOverCtrl:PopoverController,
              public alertCtrl:AlertController,
              private shareSvc:ShareService,
              public events:Events,
              private auth:AuthService,
              public modalCtrl: ModalController,
              public paymentSvc:PaymentServiceProvider
            ) {

    this.user = this.shareSvc.getUser();

    // loading.present();
    events.subscribe('user:created', (user) => {
      // console.log(user)
      this.user = user;
    });

    // this.paymentSvc.getPayments(this.page).then(res =>{
    //   // console.log(res)
    //   this.payments = res;
    // })
  }
  refresh(refresher){
    let loading = this.loadingCtrl.create({
      content: 'Carregando'
    });
    loading.present();
    this.page = 1;
    this.totalDocs = 0;
    this.paymentSvc.getPayments(this.page).then(res =>{
      console.log(res)
      loading.dismiss();
      if(refresher)
        refresher.complete();
      this.payments = res.docs;
      this.totalDocs = res.total;
    })
  }

  ionViewWillEnter(){
    console.log('load')
    this.refresh(null);

    // listen to the service worker promise in index.html to see if there has been a new update.
    // condition: the service-worker.js needs to have some kind of change - e.g. increment CACHE_VERSION.
    window['isUpdateAvailable']
      .then(isAvailable => {
        if (isAvailable) {

          console.log('Update is available');

          const alert = this.alertCtrl.create({
            title: 'New Update available!',
            message: 'Please reload the app to continue.',
            buttons: [
            {
              text: 'Reload',
              handler: () => {
                caches.delete("ionic-cache");
                location.reload(true);
              }
            }]
          });
          alert.present();
        }
      });
  }
  seePayments(){
    this.navCtrl.push(PaymentListPage);
  }
  makePayment(){
    this.navCtrl.push(MakePaymentPage);
  }
  editPayment(payment){
    if(payment.status == 'rejected')
      this.navCtrl.push(MakePaymentPage,{edit:true,payment:payment})
    else
      this.navCtrl.push(PaymentDetailPage, {id: payment._id});
  }

  nextPage(): Promise<any> {
    console.log('Begin async operation');

    return new Promise((resolve) => {

        if (this.payments.length < this.totalDocs){
          this.page++;
          let loading = this.loadingCtrl.create({
            content: 'Carregando'
          });
          loading.present();

          this.paymentSvc.getPayments(this.page).then(res =>{
            // console.log(res);
            loading.dismiss();
            this.totalDocs = res.total;
            this.payments = this.payments.concat(res.docs);
            console.log('Async operation has ended');
            resolve();
          })
        }
        else{
          setTimeout(() => {
            resolve();
          }, 500);
        }
    })
  }
}
