import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { NewImage } from './../../components/NewImage';
import { MakePayment_2Page } from './../make-payment-2/make-payment-2';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController, PopoverController, Platform, AlertController, ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { UpServiceProvider } from '../../providers/up-service';
import { ImagePicker } from '@ionic-native/image-picker';
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder, Validators } from '@angular/forms';
import { ShareService } from '../../providers/share-service';
import { Payment, MessageType } from '../../models/models';
import { GalleryModal } from 'ionic-gallery-modal';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { DecimalPipe } from '@angular/common';
import { CurrencyFormat } from '../../app/pipes/currencyFormat';

@Component({
  selector: 'page-make-payment',
  templateUrl: 'make-payment.html',
})
export class MakePaymentPage {
  public user;
  public canExit;
  public showAddress = false;
  public submitAttempt:boolean = false;
  public paymentForm;
  public payment = new Payment();

  public img:any;
  public imgName:any;
  public teste:any;
  base64Image:any;
  // private edit:boolean = false;
  private pickImage:boolean = false;
  // public media = new Array();
  // public picture = new Array();
  public audio = new Array();
  public makingOffer = false;
  public justification;
  public loadingImg:boolean = false;
  constructor(
    public camera:Camera,
    public events:Events,
    public modalCtrl:ModalController,
    public upService : UpServiceProvider,
    private imagePicker: ImagePicker,
    public popoverCtrl:PopoverController ,
    private sanitizer:DomSanitizer,
    public navCtrl: NavController,
    public platform: Platform,
    public viewCtrl:ViewController,
    public formBuilder:FormBuilder,
    public navParams: NavParams,
    public shareSvc:ShareService,
    public loadingCtrl:LoadingController,
    public toastCtrl:ToastController,
    public actionSheetCtrl:ActionSheetController,
    private decimalPipe: DecimalPipe,
    public currencyPipe: CurrencyFormat,
    public alertCtrl:AlertController) {
    // super(navCtrl,toastCtrl,actionSheetCtrl,camera,upService,events,viewCtrl);

      console.log(this.payment)
    this.user = this.shareSvc.getUser();
    let itemEdit = this.navParams.get('payment');

    if(itemEdit){
      if(itemEdit.value )
      this.paymentForm = formBuilder.group({
        value: [itemEdit.value],
        type: [itemEdit.type],
        media: [itemEdit.media],
        derivation:[itemEdit.dve]
      })
      this.payment = itemEdit;
      // this.media = itemEdit.media;
      if(itemEdit.status == "rejected"){
        this.justification = itemEdit.history[itemEdit.history.length - 1].justification;
      }
    }else{
      this.paymentForm = formBuilder.group({
        value: [,Validators.required],
        type: ["",Validators.required],
        media: ["",Validators.required],
        // media: ["",Validators.required],
        derivation:[""]
      })
    }


    this.events.subscribe("imageUploaded",(url,page) =>{
      console.log(page)
      console.log(url)
      console.log(this.payment.media)
      if(page==1){
        let auth = this.getAuthentication(url);
        console.log(auth)
        // this.media.push(url)
        // this.picture.push({url:url})
        // let media =this.payment.media  this.paymentForm.controls['media'].getValue();

        console.log(this.paymentForm)
        console.log(this.payment)
      }
    })

  }
  getAuthentication(url) {
    const prompt = this.alertCtrl.create({
      enableBackdropDismiss:false,
      title: 'Número do documento',
      inputs: [
        {
          name: 'auth',
          placeholder: 'Digite Aqui',
          type:'number'
        },
      ],
      buttons: [
        {
          text: 'Próximo',
          handler: data => {
            console.log(data)
            if(data.auth.length > 3){
              this.payment.media.push({url:url,authentication:data.auth})
              console.log(this.payment.media)
              this.paymentForm.controls['media'].setValue(this.payment.media);
            }else{
              console.log('ss')
              this.getAuthentication(url);
            }
          }
        }
      ]
    });
  prompt.present().then(() => {
    const firstInput: any = document.querySelector('ion-alert input');
    firstInput.focus();
    return;
  });
  }
  ionViewWillEnter(){
    // if(this.payment.value){
    //   let valueToStr = this.payment.value.toString();
    //   let indexDot = valueToStr.indexOf('.')
    //   if(indexDot == -1){
    //     this.payment.value = valueToStr+'00';
    //   }else if(valueToStr.length - indexDot == 2){
    //     this.payment.value = valueToStr+'0';
    //   }
    // }
  }

  onSelectChange(selectedValue) {
    if(selectedValue == 'intermunicipal' || selectedValue == 'encomendas'){

      this.paymentForm = this.formBuilder.group({
        value: [this.payment.value,Validators.required],
        type: [this.payment.type,Validators.required],
        media: [this.payment.media,Validators.required],
        derivation:["",Validators.required]
      })
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CompleteRegister');
  }
  changeAuth(item, index){
    console.log(item)
    console.log(index)
    const prompt = this.alertCtrl.create({
      enableBackdropDismiss:false,
      title: 'Número do documento',
      inputs: [
        {
          name: 'auth',
          placeholder: 'Digite Aqui',
          value:item.authentication,
          type:'number'
        },
      ],
      buttons: [
        {
          text: 'Próximo',
          handler: data => {
            console.log(data)
            if(data.auth.length > 3){
              console.log(this.payment.media)
              console.log(this.payment.media[index])
              this.payment.media[index] = {url:item.url,authentication:data.auth}
              console.log(this.payment.media[index])
              console.log(this.payment.media)
              this.paymentForm.controls['media'].setValue(this.payment.media);
            }else{
              console.log('ss')
              this.changeAuth(item, index);
            }
          }
        }
      ]
    });
    prompt.present().then(() => {
      const firstInput: any = document.querySelector('ion-alert input');
      firstInput.focus();
      return;
    });
  }

  formatMoney(value){
    if (value != undefined){

      if(typeof value == 'number'){
        value = value + '';
        value = parseFloat(value).toFixed(2);
        if (value != ""){
          value = parseFloat( value.replace(/[\D]+/g,'') );
        }
      }
      else{
        value = value + '';
        value = value.replace(/\./g, '');
        value = value.replace(new RegExp(',', 'g'), '.');
        if (value != ""){
          value = parseFloat( value.replace(/[\D]+/g,'') );
        }
      }

      value = value+'';

      var tmp = value;

      tmp += '';
      tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
      if( tmp.length > 6 ){
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
      }
      if( tmp.length > 10 ){
        tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g, ".$1.$2,$3");
      }

      return tmp;
    }
    else{
      return value;
    }
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  formatNumber(event){
    if (event != undefined){
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return event;
      }
      return event;
    }else
      return event;
  }

  // pushImage(url){
  //   this.media.push(url)
  //   this.picture.push({url:url})
  //   // let media =this.payment.media  this.paymentForm.controls['media'].getValue();
  //   this.payment.media.push(url)
  //   this.paymentForm.controls['media'].setValue(this.payment.media);
  //   console.log(this.paymentForm)
  //   console.log(this.payment)
  // }
  addMedia(){
    if(this.platform.is('core') || this.platform.is('mobileweb')) {
      var fileElem = document.getElementById("filePicker");
      fileElem.click();
    }else if(this.platform.is('ios') || this.platform.is('android')){
      this.showActionSheet();
    }else
      alert('erro')
  }

handleFileSelect(evt){

  var files = evt.target.files;
  var file = files[0];
  this.loadingImg = true;
  // let loading = this.loadingCtrl.create({
  //   content: 'Enviando mídia'
  // });
  let toast = this.toastCtrl.create({
    message: 'Enviando mídia',
    position: 'top'
  });
  toast.present()
  // loading.present();
  this.upService.uploadFilePC(file).then(res => {
    let response : any = res;
    // loading.dismiss();
    this.loadingImg = false;
    toast.dismiss();

    this.events.publish("imageUploaded",response.url,1);
  })
}

  deleteMedia(index){
    console.log(index)
    let alert = this.alertCtrl.create({
      title: 'Confirma remoção',
      message: 'Você deseja remover essa imagem ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Remover',
          handler: () => {
            // this.picture.splice(index,1)
            this.payment.media.splice(index,1)
          }
        }
      ]
    });
    alert.present();
  }

  viewPic(indexPic){
    let arrayToShow  = new Array();
    this.payment.media.forEach(pic => {
      arrayToShow.push({url:pic.url});
    });
    let modal = this.modalCtrl.create(GalleryModal, {
      photos: arrayToShow,
      initialSlide: indexPic
    });
    modal.present();
  }

  newAddress(){
    this.showAddress = true;
  }

  showActionSheet(){
    let actionSheet = this.actionSheetCtrl.create({
      title: "Selecione: ",
      buttons: [
        {
          text: 'Tirar foto',
          handler: () => {
            console.log('tirar foto')
            this.getImage(1);
          }
        },{
          text: 'Utilizar galeria',
          handler: () => {
            console.log('galeria')
            this.getImage(2)
          }
        }
      ]
    });
    actionSheet.present();
  }
  getImage(mode){
    console.log(mode)
    // return mode;

    var sourceType = mode;
      const options: CameraOptions = {
          quality: 60,
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType: sourceType,
          allowEdit: false,
          encodingType: this.camera.EncodingType.JPEG,
          saveToPhotoAlbum: false,
          correctOrientation:true,
          targetWidth: 800,
      };
      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        let toast = this.toastCtrl.create({
          message: 'Enviando mídia',
          position: 'top'
        });
        this.loadingImg = true;
        toast.present()
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        this.upService.uplodFile(base64Image).then(response =>{
          this.loadingImg = false;
          toast.dismiss();
          console.log(response)
          let res:any = response;
          this.getAuthentication(res.url)
          // this.payment.media.push(res.url)
          // if(res.success){
          //   this.events.publish("imageUploaded",res.url,page);
          // }

            // console.log('oi')
            // this.viewCtrl.dismiss();
        })
        }, (err) => {
        // Handle error
        });
  }
  checkForm(){

    this.paymentForm.controls['media'].setValue(this.payment.media);
    let edit = this.navParams.get('payment')
    if(this.paymentForm.valid){
      // this.payment.value = ; //conversao dos valores, pois a mascara retorna apenas str com ',' sendo necessário trocar por '.' e em seguida fazer a conversao para nmr
      // this.payment.value = parseFloat(this.payment.value.toString()); //conversao dos valores, pois a mascara retorna apenas str com ',' sendo necessário trocar por '.' e em seguida fazer a conversao para nmr
      this.navCtrl.push(MakePayment_2Page,{payment:this.payment,edit:edit})
      // this.viewCtrl.dismiss({piece:payment});
     }else
      this.submitAttempt = true;
  }

  // timeChanged(value){
  //   console.log(value)
  //   this.payment.value = this.decimalPipe.transform(value, '1.2-2');
  // }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
