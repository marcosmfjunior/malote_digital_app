// import { IonicImageLoader } from 'ionic-image-loader';
import { PaymentServiceProvider } from './../../providers/payment-service';
import { MakePaymentPage } from './../make-payment/make-payment';
import { ProductsService } from './../../providers/products-service';
import { ShareService } from './../../providers/share-service';
import { User, UserInfo, Payment } from './../../models/models';
import { AuthService } from './../../providers/auth-service';
import { Component,ViewChild, ElementRef } from '@angular/core';
import { NavController, ModalController, Events, AlertController, PopoverController, LoadingController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { GalleryModal } from 'ionic-gallery-modal';

@Component({
  selector: 'page-payment-detail',
  templateUrl: 'payment-detail.html'
})
export class PaymentDetailPage {

  public payment: any;

  constructor(public navCtrl: NavController,
              public navParam: NavParams,
              public loadingCtrl:LoadingController,
              public paymentSvc:PaymentServiceProvider,
              public modalCtrl:ModalController
            ) {

    let loading = this.loadingCtrl.create({
      content: 'Carregando'
    });
    loading.present();

    this.paymentSvc.getPayment(navParam.data.id).then(res =>{
      console.log(res);
      loading.dismiss();
      this.payment = res;
    })
  }
  viewPicPayment(indexPic,arraySelected){
    let arrayToShow  = new Array();
    arraySelected.forEach(pic => {
      console.log(pic)
      arrayToShow.push({url:pic.url});
    });
    let modal = this.modalCtrl.create(GalleryModal, {
      photos: arrayToShow,
      initialSlide: indexPic
    });
    modal.present();
  }
  
  viewPicNote(indexPic,arraySelected){
    let arrayToShow  = new Array();
    arraySelected.forEach(pic => {
      console.log(pic)
      arrayToShow.push({url:pic});
    });
    let modal = this.modalCtrl.create(GalleryModal, {
      photos: arrayToShow,
      initialSlide: indexPic
    });
    modal.present();
  }

}
