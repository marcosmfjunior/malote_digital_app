import { PaymentServiceProvider } from './../../providers/payment-service';
import { MakePaymentPage } from './../make-payment/make-payment';
import { ProductsService } from './../../providers/products-service';
import { ShareService } from './../../providers/share-service';
import { User, UserInfo, DaysSelect, Payment } from './../../models/models';
import { AuthService } from './../../providers/auth-service';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, ModalController, Events, AlertController, PopoverController, LoadingController, ToastController } from 'ionic-angular';
import { PaymentDetailPage } from './payment-detail';
import { CalendarModalOptions, CalendarModal } from 'ion2-calendar';

@Component({
  selector: 'page-payment-list',
  templateUrl: 'payment-list.html'
})
export class PaymentListPage {

  public payments = new Array();
  public originalPaymentes;
  public totalDocs = 0;
  public filters: any = {
    page: 1
  };

  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public paymentSvc: PaymentServiceProvider,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController
  ) {

    let loading = this.loadingCtrl.create({
      content: 'Carregando'
    });
    loading.present();

    this.paymentSvc.getPaymentsAccepted(this.filters).then(res => {
      // console.log(res)
      loading.dismiss();
      this.payments = res.docs;
      this.totalDocs = res.total;
      this.originalPaymentes = JSON.parse(JSON.stringify(res.docs));
    })
  }

  seePayment(payment) {
    this.navCtrl.push(PaymentDetailPage, { id: payment._id });
  }
  openCalendar() {
    this.showToast();

    let fromDate = new Date("Mon, 01 Jan 2018 15:19:41 +0000");
    const options: CalendarModalOptions = {
      daysConfig: this.getSelectedDays(),
      title: 'Selecione um período ',
      pickMode: 'range',
      weekdays: ["D", "S", "T", "Q", "Q", "S", "S"],
      // closeLabel: " X ",
      color: "primary",
      doneIcon: true,
      closeIcon:true,
      defaultScrollTo: new Date(),
      showAdjacentMonthDay: true,
      showYearPicker: true,
      from: fromDate,
      // defaultScrollTo:new Date(),
      to: new Date(),
    };
    let myCalendar = this.modalCtrl.create(CalendarModal, {
      type: "moment",
      options: options
    });

    myCalendar.present();

    myCalendar.onDidDismiss((dates: any, type: string) => {

      this.filters = {
        page: 1
      };

      if (dates != undefined) {
        if (dates.from != undefined && dates.to != undefined) {
          this.filters.start = dates.from.string;
          this.filters.end = dates.to.string;
        }
      }

      let loading = this.loadingCtrl.create({
        content: 'Carregando'
      });
      loading.present();

      // this.page = 1;
      this.paymentSvc.getPaymentsAccepted(this.filters).then(res => {
        loading.dismiss();
        this.payments = res.docs;
        this.totalDocs = res.total;
        // this.originalPaymentes =  JSON.parse(JSON.stringify(res));
      })
      // busca aqui
    })
  }
  filterPayments(datesSelected) {

    this.payments = new Array();
    // let originalPaymentes = JSON.parse(JSON.stringify(this.originalPaymentes));
    this.originalPaymentes.forEach((payment, indexPayment) => {
      datesSelected.forEach(day => {
        if (new Date(day.dateObj).getTime() === new Date(payment.createdAt).getTime()) {
          this.payments.push(JSON.parse(JSON.stringify(this.originalPaymentes[indexPayment])))
        }
      });
    });
  }
  getSelectedDays() {
    console.log(this.originalPaymentes)
    let days = new Array();
    console.log(days)
    this.originalPaymentes.forEach(payment => {
      if (days.indexOf(payment.createdAt) == -1) {
        let config = { marked: true, date: payment.createdAt, subTitle: '✔' };
        // let config = {marked:true,date:new Date(payment.createdAt)};
        days.push(config);
      }
      // days.push(payment.days[0].day_selected);
    });
    console.log(days)
    return days;
  }
  showToast() {
    let toast = this.toastCtrl.create({
      message: 'Selecione duas datas',
      position: 'bottom',
      duration: 3000,
    });
    toast.present();
  }

  nextPage(): Promise<any> {
    console.log('Begin async operation');

    return new Promise((resolve) => {

      if (this.payments.length < this.totalDocs) {
        this.filters.page++;
        let loading = this.loadingCtrl.create({
          content: 'Carregando'
        });
        loading.present();

        this.paymentSvc.getPaymentsAccepted(this.filters).then(res => {
          // console.log(res);
          loading.dismiss();
          this.totalDocs = res.total;
          this.payments = this.payments.concat(res.docs);
          console.log('Async operation has ended');
          resolve();
        })
      }
      else {
        setTimeout(() => {
          resolve();
        }, 500);
      }
    })
  }
}
