import { CurrencyFormat } from './../../app/pipes/currencyFormat';
import { CurrencyPipe } from '@angular/common';
import { UpServiceProvider } from './../../providers/up-service';
import { PaymentServiceProvider } from './../../providers/payment-service';
import { NotesPopOver } from './../notes-pop-over/notes-pop-over';
import { Payment, DaysSelect, Notes } from './../../models/models';
import { CalendarModule,CalendarModal, CalendarModalOptions, CalendarResult } from 'ion2-calendar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, PopoverController, ToastController, Events, AlertController, ItemSliding, Item } from 'ionic-angular';
import { NewImage } from '../../components/NewImage';
import { Camera } from '@ionic-native/camera';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

@Component({
  selector: 'page-make-payment-2',
  templateUrl: 'make-payment-2.html',
})
export class MakePayment_2Page{
  public payment: Payment;
  public edit;
  activeItemSliding: ItemSliding = null;
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public paymentSvc:PaymentServiceProvider,
    public upService:UpServiceProvider,
    public events:Events,
    public popoverCtrl: PopoverController,
    public loadingCtrl: LoadingController,
    public alertCtrl:AlertController
  ) {

      this.edit = this.navParams.get('edit');
      this.payment = this.navParams.get('payment');
      console.log(this.payment)
      // this.payment = new Payment();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MakePayment_2Page');
  }
  openCalendar() {


    let fromDate = new Date("Mon, 01 Jan 2018 15:19:41 +0000");
    const options: CalendarModalOptions = {
      title: 'Selecione o período',
      pickMode: 'multi',
      weekdays: ["D","S","T","Q","Q","S","S"],
      closeIcon:false,
      closeLabel:"",
      color:"primary",
      doneIcon:true,
      from: fromDate,
      defaultScrollTo:new Date(),
      to: new Date(),
      defaultDates:this.getSelectedDays()
    };
    let myCalendar =  this.modalCtrl.create(CalendarModal, {
      type:"moment",
      options: options
    });

    myCalendar.present();

    myCalendar.onDidDismiss((dates: any, type: string) => {
      let newDays : DaysSelect[]= new Array();
      console.log(dates)
      dates.forEach(dateSelected => {
        let date = new DaysSelect();
        // date.notes =  new Array();
        date.day_selected = dateSelected.dateObj;
        newDays.push(date);
      });
      this.checkDates(newDays);
      // let newDays : DaysSelect[]= new Array();
      // console.log(dates)
      // dates.forEach(dateSelected => {
      //   let date = new DaysSelect();
      //   date.day_selected = dateSelected.dateObj;
      //   if(this.checkSeletecedDays(dateSelected.dateObj))
      //     this.payment.days.push(date);
      // });
    })
  }
  checkDates(newDays){
    this.payment.days.forEach((day,indexDay) => {
      newDays.forEach(newDay => {
        console.log(newDay)
        console.log(day.day_selected)
        if(new Date(newDay.day_selected).getTime() === new Date(day.day_selected).getTime()){
          console.log('entrou aqui')
          newDay.notes =  JSON.parse(JSON.stringify(this.payment.days[indexDay].notes));
        }
      })
    })
    this.payment.days =  JSON.parse(JSON.stringify(newDays));
  }
  checkSeletecedDays(selected){
    console.log(selected)
    this.payment.days.forEach(day => {
      console.log(new Date(day.day_selected).getTime())
      console.log(new Date(selected).getTime())
      if(new Date(day.day_selected).getTime() === new Date(selected).getTime()){
        return false;
      }
    });
  }
  getSelectedDays(){
    if(this.edit){
      let days = new Array();
      this.payment.days.forEach(day => {
        days.push(day.day_selected);
      });
      return days;
    }else
      return null;
  }

  addAdjust(daySelected,ev){
    let popover = this.popoverCtrl.create(NotesPopOver,{edit:false},{enableBackdropDismiss:false,showBackdrop:true});
    popover.present({
      // ev: ev
    });
    popover.onDidDismiss(res => {
      console.log('add')
      if(res){
        res.value = res.value.replace(/\./g, '');
        res.value = res.value.replace(new RegExp(',', 'g'), '.');
        res.value = parseFloat( res.value );

        let index = this.payment.days.indexOf(daySelected);
        this.payment.days[index].notes.push(res);
      }
    })
  }
  editAdjust(note,daySelected,ev){
    if(this.activeItemSliding) {
      this.activeItemSliding.close();
      this.activeItemSliding = null;
    }
    console.log(note)
    let popover = this.popoverCtrl.create(NotesPopOver,{edit:true,note:note},{enableBackdropDismiss:true,showBackdrop:true});
    popover.present({
      // ev: ev
    });
    popover.onDidDismiss(res => {
      console.log('edit')
      console.log(res)
      if(res){
        res.value = res.value.replace(/\./g, '');
        res.value = res.value.replace(new RegExp(',', 'g'), '.');
        res.value = parseFloat( res.value );

        let indexDay = this.payment.days.indexOf(daySelected);
        let indexNote = this.payment.days[indexDay].notes.indexOf(note);
        this.payment.days[indexDay].notes[indexNote] = res;
      }
    })
  }
  deleteAdjust(note,daySelected){

    let alert = this.alertCtrl.create({
      title: 'Confirma remoção',
      message: 'Você deseja remover esse ajuste ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Remover',
          handler: () => {
            let indexDay = this.payment.days.indexOf(daySelected);
            let indexNote = this.payment.days[indexDay].notes.indexOf(note);
            this.payment.days[indexDay].notes.splice(indexNote,1);
          }
        }
      ]
    });
    alert.present();
    if(this.activeItemSliding) {
      this.activeItemSliding.close();
      this.activeItemSliding = null;
    }
  }


  sendPayment(){
    let alert = this.alertCtrl.create({
      title: 'Confirmação de envio',
      message: 'Você deseja enviar as informações ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Enviar',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Salvando'
            });
            loading.present();

            this.payment.value = this.payment.value + '';
            this.payment.value = this.payment.value.replace(/\./g, '');
            this.payment.value = this.payment.value.replace(new RegExp(',', 'g'), '.');

            this.payment.value = parseFloat( this.payment.value );
            this.paymentSvc.sendPayment(this.payment).then(res =>{
              loading.dismiss();
              // console.log(res)
              if(res.success)
                this.navCtrl.popToRoot()
            })
          }
        }
      ]
    });
    alert.present();
  }
  editPayment(id){
    let alert = this.alertCtrl.create({
      title: 'Confirma envio',
      message: 'Você deseja editar e salvar esse ajuste ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Salvar',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Salvando'
            });
            loading.present();

            this.payment.value = this.payment.value + '';
            this.payment.value = this.payment.value.replace(/\./g, '');
            this.payment.value = this.payment.value.replace(new RegExp(',', 'g'), '.');

            this.payment.value = parseFloat( this.payment.value );
            this.paymentSvc.editPayment(id,this.payment).then(res =>{
              loading.dismiss();

              if(res.success)
                this.navCtrl.popToRoot()
            })
          }
        }
      ]
    });
    alert.present();
  }
  openOption(itemSlide: ItemSliding, item: Item) {
    console.log('opening item slide..');

    if(this.activeItemSliding!==null) //use this if only one active sliding item allowed
     this.closeOption();

    this.activeItemSliding = itemSlide;

    let swipeAmount = 194; //set your required swipe amount
    itemSlide.startSliding(swipeAmount);
    itemSlide.moveSliding(swipeAmount);

    itemSlide.setElementClass('active-options-right', true);
    itemSlide.setElementClass('active-swipe-right', true);

    item.setElementStyle('transition', null);
    item.setElementStyle('transform', 'translate3d(-'+swipeAmount+'px, 0px, 0px)');
   }

   closeOption() {
    console.log('closing item slide..');

    if(this.activeItemSliding) {
     this.activeItemSliding.close();
     this.activeItemSliding = null;
    }
   }
}
